# window.py
#
# Copyright 2020 Jan-Michael Brummer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Handy, GdkPixbuf, Gdk, Pango, Gio, GLib
from threading import Timer
from gettext import gettext as _


class BankingAccountRow(Gtk.ListBoxRow):
    def __init__(self, account, **kwargs):
        super().__init__(**kwargs)

        self.account = account

        grid = Gtk.Grid()
        grid.set_hexpand(False)
        grid.set_hexpand_set(True)
        grid.set_column_spacing(12)
        grid.set_border_width(6)
        self.add(grid)

        if account['association'] == 'dsgv':
            pixbuf = GdkPixbuf.Pixbuf.new_from_resource_at_scale(
                "/org/tabos/banking/resources/dsgv.svg", 32, 32, True)
        elif account['association'] == 'bdb':
            pixbuf = GdkPixbuf.Pixbuf.new_from_resource_at_scale(
                "/org/tabos/banking/resources/bdb.jpg", 32, 32, True)
        elif account['association'] == 'bvr':
            pixbuf = GdkPixbuf.Pixbuf.new_from_resource_at_scale(
                "/org/tabos/banking/resources/bvr.svg", 32, 32, True)

        image = Gtk.Image()
        image.set_from_pixbuf(pixbuf)
        grid.attach(image, 0, 0, 1, 3)

        product_name_label = Gtk.Label()
        product_name_label.set_markup('<b>' + account['product_name'] + '</b>')
        product_name_label.set_line_wrap(True)
        product_name_label.set_halign(Gtk.Align.START)
        grid.attach(product_name_label, 1, 0, 1, 1)

        account_label = Gtk.Label(account['iban'])
        account_label.set_line_wrap(True)
        account_label.set_halign(Gtk.Align.START)
        grid.attach(account_label, 1, 1, 1, 1)

        owner_name_label = Gtk.Label()
        owner_name_label.set_markup(
            '<small>' + account['owner_name'] + '</small>')
        owner_name_label.set_line_wrap(True)
        owner_name_label.set_halign(Gtk.Align.START)
        grid.attach(owner_name_label, 1, 2, 2, 1)

        balance_label = Gtk.Label(
            account['balance'] + ' ' + account['currency'])
        balance_label.set_hexpand(True)
        balance_label.set_halign(Gtk.Align.END)
        if float(account['balance']) < 0.0:
            balance_label.set_name('debit')
        grid.attach(balance_label, 2, 1, 1, 1)

    def get_iban(self):
        return self.account['iban']

    def get_account(self):
        return self.account


class BankingTransactionRow(Gtk.ListBoxRow):
    def __init__(self, transaction, **kwargs):
        super().__init__(**kwargs)

        self.transaction = transaction

        grid = Gtk.Grid()
        grid.set_hexpand(True)
        grid.set_column_spacing(12)
        grid.set_border_width(6)
        self.add(grid)

        applicant_name_label = Gtk.Label()
        if transaction['applicant_name']:
            applicant_name_label.set_markup(
                '<b>' + transaction['applicant_name'] + '</b>')
        else:
            applicant_name_label.set_markup(
                '<b>' + transaction['posting_text'] + '</b>')
        applicant_name_label.set_hexpand(True)
        applicant_name_label.set_line_wrap(True)
        applicant_name_label.set_ellipsize(Pango.EllipsizeMode.END)
        applicant_name_label.set_xalign(0.0)
        applicant_name_label.set_halign(Gtk.Align.START)
        grid.attach(applicant_name_label, 0, 0, 1, 1)

        sub_label = Gtk.Label(
            transaction['date'] + ' - ' + transaction['posting_text'])
        sub_label.set_line_wrap(True)
        sub_label.set_sensitive(False)
        sub_label.set_xalign(0.0)
        sub_label.set_halign(Gtk.Align.START)
        sub_label.set_ellipsize(Pango.EllipsizeMode.END)
        grid.attach(sub_label, 0, 1, 1, 1)

        amount_label = Gtk.Label(transaction['amount']['amount'])

        if float(transaction['amount']['amount']) >= 0.0:
            amount_label.set_name('credit')

        amount_label.set_hexpand(True)
        amount_label.set_halign(Gtk.Align.END)
        amount_label.set_valign(Gtk.Align.START)
        grid.attach(amount_label, 1, 0, 1, 1)

    def _get_value(self, key):
        val = self.transaction.get(key)
        if val and val.strip():
            return val

        return ''

    def get_date(self):
        return self._get_value('date')

    def get_entry_date(self):
        return self._get_value('entry_date')

    def get_applicant_name(self):
        return self._get_value('applicant_name')

    def get_posting_text(self):
        return self._get_value('posting_text')

    def get_amount(self):
        return self.transaction['amount']['amount'] or ''

    def get_iban(self):
        return self._get_value('applicant_iban')

    def get_bic(self):
        return self._get_value('applicant_bin')

    def get_purpose(self):
        return self._get_value('purpose')

    def get_applicant_creditor_id(self):
        return self._get_value('applicant_creditor_id')

    def get_additional_position_reference(self):
        return self._get_value('additional_position_reference')

    def get_end_to_end_reference(self):
        return self._get_value('end_to_end_reference')


@Gtk.Template(resource_path='/org/tabos/banking/ui/details.ui')
class BankingTransactionDetails(Handy.ApplicationWindow):
    __gtype_name__ = 'BankingTransactionDetails'

    booking_date_label = Gtk.Template.Child()
    value_label = Gtk.Template.Child()
    amount_label = Gtk.Template.Child()
    transaction_type_label = Gtk.Template.Child()

    name_label = Gtk.Template.Child()
    iban_label = Gtk.Template.Child()
    bic_label = Gtk.Template.Child()

    reference_label = Gtk.Template.Child()

    creditor_id_label = Gtk.Template.Child()
    mandate_reference_label = Gtk.Template.Child()
    end_to_end_reference_label = Gtk.Template.Child()

    def __init__(self, transaction, **kwargs):
        super().__init__(**kwargs)
        self.booking_date_label.set_text(transaction.get_date())
        self.value_label.set_text(transaction.get_entry_date())
        self.amount_label.set_text(transaction.get_amount())
        self.transaction_type_label.set_text(transaction.get_posting_text())

        self.name_label.set_text(transaction.get_applicant_name())
        self.iban_label.set_text(transaction.get_iban())
        self.bic_label.set_text(transaction.get_bic())

        self.reference_label.set_text(transaction.get_purpose())

        self.creditor_id_label.set_text(
            transaction.get_applicant_creditor_id())
        self.mandate_reference_label.set_text(
            transaction.get_additional_position_reference())
        self.end_to_end_reference_label.set_text(
            transaction.get_end_to_end_reference())


@Gtk.Template(resource_path='/org/tabos/banking/ui/window.ui')
class BankingWindow(Handy.ApplicationWindow):
    __gtype_name__ = 'BankingWindow'

    Handy.init()

    content_stack = Gtk.Template.Child()
    password_entry = Gtk.Template.Child()
    password_button = Gtk.Template.Child()
    account_listbox = Gtk.Template.Child()
    transaction_listbox = Gtk.Template.Child()
    total_balance_label = Gtk.Template.Child()
    default_header_bar = Gtk.Template.Child()
    content_box = Gtk.Template.Child()
    back_button = Gtk.Template.Child()
    search_bar = Gtk.Template.Child()
    search_entry = Gtk.Template.Child()
    start_assistant_button = Gtk.Template.Child()
    lock_timer = NotImplemented

    def __init__(self, backend, server='', **kwargs):
        super().__init__(**kwargs)

        css = """
        #credit {
            color: green;
        }
        """
        style_provider = Gtk.CssProvider()
        style_provider.load_from_data(bytes(css.encode()))
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(), style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

        self.content_stack.set_visible_child_name('welcome')
        self.backend = backend

        self.backend.connect('accounts-refreshed', self.add_accounts)
        self.account_listbox.connect('row-activated', self.account_activated)
        self.account_listbox.set_header_func(self._header_func)
        self.account_listbox.set_sort_func(self._account_sort)
        self.account_listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.transaction_listbox.set_header_func(self._header_func)
        self.transaction_listbox.set_sort_func(self._transaction_sort)
        self.transaction_listbox.set_filter_func(self._transaction_filter)
        self.transaction_listbox.connect('row-activated',
                                         self._on_row_activated)
        self.transaction_listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.back_button.connect('clicked', self.back_button_clicked)
        self.search_entry.connect(
            'search-changed',
            self.search_entry_search_changed)

        self.search_text = None

        self.search_bar.connect_entry(self.search_entry)
        self.connect('key-press-event', self._on_key_pressed)

        self.password_entry.connect('icon-release',
                                    self._onpassword_entry_icon_released)
        self.password_entry.set_activates_default(True)
        self.password_button.connect('clicked',
                                     self._onpassword_button_clicked)

        accounts = self.backend.get_accounts()

        if accounts:
            self.show_unlock_dialog()
            self.add_accounts(None)
        else:
            Gtk.Widget.grab_focus(self.start_assistant_button)

        self.content_box.set_visible_child_name('sidebar')

    def show_unlock_dialog(self):
        Gtk.Widget.grab_focus(self.password_entry)
        self.password_entry.set_text('')
        Gtk.Entry.set_visibility(self.password_entry, False)
        self.set_default(self.password_button)
        self.content_stack.set_visible_child_name('unlock')

    def _onpassword_entry_icon_released(self, widget, icon, event):
        if icon != Gtk.EntryIconPosition.SECONDARY:
            return

        if Gtk.Entry.get_visibility(self.password_entry):
            Gtk.Entry.set_visibility(self.password_entry, False)
        else:
            Gtk.Entry.set_visibility(self.password_entry, True)

    def send_notification(self, title, text):
        notification = Gio.Notification.new(title)
        notification.set_body(text)
        Gio.Application.get_default().send_notification(None, notification)

    def lock_timeout(self):
        self.show_unlock_dialog()
        self.send_notification(_("Database locked"),
                               _("Banking safe locked due to inactivity"))

    def start_lock_timer(self):
        if self.lock_timer is not NotImplemented:
            self.lock_timer.cancel()
        timeout = 2 * 60
        self.lock_timer = Timer(timeout,
                                GLib.idle_add,
                                args=[self.lock_timeout])
        self.lock_timer.start()

    def _onpassword_button_clicked(self, widget):
        if self.backend.is_safe_password_valid(self.password_entry.get_text()):
            self.content_stack.set_visible_child_name('content')
            self.start_lock_timer()

    def _on_row_activated(self, box, row):
        self.start_lock_timer()
        if not row:
            return

        details = BankingTransactionDetails(row)
        details.set_transient_for(self)
        details.show_all()

    def _on_key_pressed(self, window, event):
        if self.content_stack.get_visible_child_name == 'content':
            self.start_lock_timer()
            return self.search_bar.handle_event(event)

        return False

    def search_entry_search_changed(self, entry):
        self.search_text = entry.get_text()
        self.transaction_listbox.invalidate_filter()

    def back_button_clicked(self, button):
        self.content_box.set_visible_child_name('sidebar')

    def _account_sort(self, row1, row2):
        iban1 = row1.get_iban()
        iban2 = row2.get_iban()

        if iban1 < iban2:
            return -1
        if iban1 > iban2:
            return 1
        return 0

    def _transaction_sort(self, row1, row2):
        date1 = row1.get_date()
        date2 = row2.get_date()

        if date1 < date2:
            return 1
        if date1 > date2:
            return -1
        return 0

    def _transaction_filter(self, row):
        ret = self.search_text.lower() in row.get_applicant_name().lower() \
            if self.search_text else True

        if not ret:
            ret = self.search_text.lower() in row.get_posting_text().lower() \
                if self.search_text else True

        return ret

    def _header_func(self, row, before):
        if not before:
            row.set_header(None)

        if row.get_header():
            return

        header = Gtk.Separator()
        header.show()
        row.set_header(header)

    def account_activated(self, box, row):
        self.start_lock_timer()
        childrens = self.transaction_listbox.get_children()
        for child in childrens:
            child.destroy()

        if not row:
            return

        account = row.get_account()

        self.default_header_bar.set_title(account['product_name'])
        self.default_header_bar.set_subtitle(account['iban'])

        if account['transactions']:
            for transaction in account['transactions']:
                row = BankingTransactionRow(transaction)
                self.transaction_listbox.add(row)

            self.transaction_listbox.show_all()
        self.content_box.set_visible_child_name('content')

    def add_accounts(self, inst):
        total_balance = 0

        childrens = self.account_listbox.get_children()
        for child in childrens:
            child.destroy()

        for account in self.backend.get_accounts():
            row = BankingAccountRow(account)
            self.account_listbox.add(row)

            total_balance = total_balance + float(account['balance'])

        if self.backend.get_accounts():
            self.total_balance_label.set_text(
                str(round(total_balance, 2)) + ' ' + account['currency'])

        self.account_listbox.show_all()
