# assistant.py
#
# Copyright 2020 Jan-Michael Brummer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Assistant window: Configure blz, user, password and server needed to access
online banking information based on fints standard.
"""

from gi.repository import Gtk


@Gtk.Template(resource_path='/org/tabos/banking/ui/assistant.ui')
class BankingAssistant(Gtk.ApplicationWindow):
    """
    Create a new bank account configuration
    """
    __gtype_name__ = 'BankingAssistant'

    bank_entry = Gtk.Template.Child()
    user_entry = Gtk.Template.Child()
    password_entry = Gtk.Template.Child()
    safe_password_entry = Gtk.Template.Child()
    server_entry = Gtk.Template.Child()
    quit_button = Gtk.Template.Child()
    done_button = Gtk.Template.Child()

    def __init__(self, backend, **kwargs):
        super().__init__(**kwargs)

        self.backend = backend
        self.user_entry.set_text(self.backend.get_user())
        self.bank_entry.set_text(self.backend.get_blz())
        self.server_entry.set_text(self.backend.get_server())
        self.password_entry.set_text(self.backend.get_password())

        self.done_button.connect_after('clicked', self.done_clicked)
        self.quit_button.connect('clicked', self.quit_clicked)
        self.bank_entry.connect('search-changed',
                                self.bank_entry_search_changed)

    def bank_entry_search_changed(self, entry):
        server = self.backend.find_server(self.bank_entry.get_text())

        if server:
            self.server_entry.set_text(server)

    def quit_clicked(self, button):
        """
        Destroy assistant window
        :param button: quit button
        """
        self.destroy()

    def done_clicked(self, button):
        """
        Store user input in gschema and close assistant
        :param button: done button
        """
        self.backend.set_blz(self.bank_entry.get_text())
        self.backend.set_user(self.user_entry.get_text())
        self.backend.set_server(self.server_entry.get_text())
        self.backend.set_safe_password(self.safe_password_entry.get_text())
        self.backend.set_password(self.password_entry.get_text())

        self.destroy()
