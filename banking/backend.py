# backend.py
#
# Copyright 2020 Jan-Michael Brummer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import json
import mt940
import time
import csv
import os
from gettext import gettext as _
from fints.client import FinTS3PinTanClient, NeedTANResponse
from gi.repository import Gtk, Gio, GObject, GLib

import secrets
from base64 import urlsafe_b64encode as b64e, urlsafe_b64decode as b64d
from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

import hashlib

BASE_KEY = "org.tabos.banking"

key_backend = default_backend()
iterations = 100_000


def _derive_key(
        password: bytes,
        salt: bytes,
        iterations: int = iterations) -> bytes:

    """Derive a secret key from a given password and salt"""
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(), length=32, salt=salt,
        iterations=iterations, backend=key_backend)
    return b64e(kdf.derive(password))


def password_encrypt(
        message: bytes,
        password: str,
        iterations: int = iterations) -> bytes:

    salt = secrets.token_bytes(16)
    key = _derive_key(password.encode(), salt, iterations)
    return b64e(
        b'%b%b%b' % (
            salt,
            iterations.to_bytes(4, 'big'),
            b64d(Fernet(key).encrypt(message)),
        )
    )


def password_decrypt(token: bytes, password: str) -> bytes:
    decoded = b64d(token)
    salt, iter, token = decoded[:16], decoded[16:20], b64e(decoded[20:])
    iterations = int.from_bytes(iter, 'big')
    key = _derive_key(password.encode(), salt, iterations)
    return Fernet(key).decrypt(token)


class TanDialog(Gtk.MessageDialog):
    def __init__(self, challenge):
        app = Gio.Application.get_default()
        parent = Gtk.Application.get_active_window(app)
        Gtk.MessageDialog.__init__(self,
                                   transient_for=parent,
                                   flags=Gtk.DialogFlags.MODAL
                                   | Gtk.DialogFlags.DESTROY_WITH_PARENT,
                                   message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.OK_CANCEL,
                                   title=_("A TAN is required"),
                                   text=challenge)

        self.set_default_response(Gtk.ResponseType.OK)

        self.tan_entry = Gtk.Entry()
        self.tan_entry.set_halign(Gtk.Align.CENTER)
        self.tan_entry.set_activates_default(True)
        self.tan_entry.set_input_purpose(Gtk.InputPurpose.DIGITS)

        box = self.get_content_area()
        box.add(self.tan_entry)
        self.show_all()


class BankingBackend(GObject.Object):
    @GObject.Signal()
    def accounts_refreshed(self):
        self.load_accounts()

    def __init__(self):
        GObject.GObject.__init__(self)
        self.fints = None
        self.settings = Gio.Settings.new(BASE_KEY)
        self.blz = self.settings.get_string('blz')
        self.user = self.settings.get_string('user')
        self.server = self.settings.get_string('server')
        self.password = ""

        self.load_accounts()

    def is_safe_password_valid(self, safe_password):
        safe_password_hash = self.settings.get_string('safe-password-hash')
        str_hash = hashlib.md5(safe_password.encode('utf-8')).hexdigest()
        return safe_password_hash == str_hash

    def find_server(self, user_blz):
        gbytes = Gio.resources_lookup_data(
            '/org/tabos/banking/resources/fints_institute.csv',
            Gio.ResourceLookupFlags.NONE)
        self.fints_db = csv.DictReader(
            gbytes.get_data().decode("iso-8859-1").splitlines(),
            delimiter=';')

        for row in self.fints_db:
            blz = row['BLZ']
            if blz == user_blz:
                return row['PIN/TAN-Zugang URL']

        return None

    def get_association(self):
        gbytes = Gio.resources_lookup_data(
            '/org/tabos/banking/resources/fints_institute.csv',
            Gio.ResourceLookupFlags.NONE)
        self.fints_db = csv.DictReader(
            gbytes.get_data().decode("iso-8859-1").splitlines(),
            delimiter=';')

        for row in self.fints_db:
            blz = row['BLZ']
            if blz == self.blz:
                return row['Organisation'].lower()

        return None

    def load_accounts(self):
        self.accounts = []

        data_dir = GLib.get_user_data_dir() + "/banking/"
        for root, dirs, files in os.walk(data_dir):
            for name in dirs:
                file = os.path.join(root, name, 'settings.json')
                if not os.path.exists(file):
                    continue

                with open(file, "r") as infile:
                    account = json.load(infile)

                file = os.path.join(root, name, 'transactions.json')
                if not os.path.exists(file):
                    continue

                transactions = None

                with open(file, "r") as infile:
                    try:
                        transactions = json.load(infile)
                    except ValueError:
                        transactions = "[]"

                account['transactions'] = transactions
                self.accounts.append(account)

    def get_user(self):
        return self.user

    def get_blz(self):
        return self.blz

    def get_server(self):
        return self.server

    def get_password(self):
        return self.password or ""

    def set_user(self, user):
        self.settings.set_string('user', user)
        self.user = user

    def set_blz(self, blz):
        self.settings.set_string('blz', blz)
        self.blz = blz

    def set_server(self, server):
        self.settings.set_string('server', server)
        self.server = server

    def set_safe_password(self, safe_password):
        """
        Safe password is kept in memory and will be stored hashed in settings
        """
        str_hash = hashlib.md5(safe_password.encode('utf-8')).hexdigest()
        self.settings.set_string('safe-password-hash', str_hash)
        self.safe_password = safe_password

    def set_password(self, password):
        encrypted_password = password_encrypt(password.encode(),
                                              self.safe_password).decode()
        self.settings.set_string('password', encrypted_password)
        self.password = password

    def get_accounts(self):
        return self.accounts

    def credentials_valid(self):
        return \
            self.user != '' and \
            self.blz != '' and \
            self.server != '' and \
            self.password != ''

    def ask_for_tan(self, response):
        tan_dialog = TanDialog(response.challenge)
        retval = tan_dialog.run()

        if retval == Gtk.ResponseType.OK:
            tan = tan_dialog.tan_entry.get_text()
            print(tan)
            tan_dialog.destroy()
            return self.fints.send_tan(response, tan)

        tan_dialog.destroy()

        return None

    def refresh_idle(self):
        if not self.fints:
            print('Connecting to server...')
            self.fints = FinTS3PinTanClient(
                self.blz,
                self.user,
                self.password,
                self.server,
                product_id='AA3B821AAECEA62FB87C27EF3')

        with self.fints:
            if self.fints.init_tan_response:
                print("A TAN is required",
                      self.fints.init_tan_response.challenge)

                tan = input('Please enter TAN:')
                self.fints.send_tan(self.fints.init_tan_response, tan)

            info = self.fints.get_information()
            sepa_accounts = self.fints.get_sepa_accounts()

            if info:
                print('Got generic bank information')
                if 'accounts' in info:
                    print('Num accounts: ' + str(len(info['accounts'])))

            for account in info['accounts']:
                # Account info
                account_info = {}
                account_info['association'] = self.get_association()
                account_info['product_name'] = account['product_name']
                account_info['currency'] = account['currency']
                account_info['iban'] = account['iban']
                account_info['owner_name'] = ', '.join(account['owner_name'])
                account_info['last_updated'] = str(time.time())

                print('Get balance for: ' + account_info['product_name'])

                # Get balance
                sepa_access = None
                for sepa_account in sepa_accounts:
                    if sepa_account.iban == account['iban']:
                        sepa_access = sepa_account
                        print("SEPA access found")
                        break
                if not sepa_access:
                    print('No SEPA access found, returning')
                    continue

                balance = self.fints.get_balance(sepa_access)
                while isinstance(balance, NeedTANResponse):
                    balance = self.ask_for_tan(balance)
                account_info['balance'] = str(balance.amount.amount)

                account_dir = os.path.join(GLib.get_user_data_dir(),
                                           'banking',
                                           account_info['iban'])
                account_settings = os.path.join(account_dir, 'settings.json')

                print('Storing generic settings to: ' + account_settings)
                if not os.path.exists(account_dir):
                    os.makedirs(account_dir)

                with open(account_settings, 'w') as outfile:
                    json.dump(account_info, outfile)

                # Get transactions
                print('Get transactions')
                account_transactions = os.path.join(
                    account_dir,
                    'transactions.json')

                transactions = self.fints.get_transactions(
                    sepa_access,
                    datetime.date.today() - datetime.timedelta(days=120),
                    datetime.date.today())

                while isinstance(transactions, NeedTANResponse):
                    transactions = self.ask_for_tan(transactions)

                if transactions:
                    print('Storing transactions to: ' + account_transactions)
                    dump = json.dumps(
                        transactions,
                        indent=4,
                        cls=mt940.JSONEncoder)

                    with open(account_transactions, "w") as outfile:
                        outfile.write(dump)
                elif not os.path.exists(account_transactions):
                    with open(account_transactions, "w") as outfile:
                        outfile.write("[]")

        print('Fire account_refreshed signal')
        self.emit('accounts_refreshed')

    def refresh_accounts(self):
        print('refresh_accounts called')
        if self.credentials_valid():
            GLib.idle_add(self.refresh_idle)
