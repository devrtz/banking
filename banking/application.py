# application.py
#
# Copyright 2020 Jan-Michael Brummer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Initialize banking application.
"""

import gi

gi.require_version('Gtk', '3.0')
gi.require_version('Handy', '1')

from gi.repository import Gtk, Gio

from .assistant import BankingAssistant
from .window import BankingWindow
from .backend import BankingBackend


@Gtk.Template(resource_path='/org/tabos/banking/ui/about_dialog.ui')
class AboutDialog(Gtk.AboutDialog):
    """About dialog"""

    __gtype_name__ = 'AboutDialog'

    def __init__(self):
        super().__init__()

        self.connect("response", self._about_response)

    def _about_response(self, klass, data=None):
        klass.destroy()


class Application(Gtk.Application):
    def __init__(self, application_id):
        """
        Initialize main:
          - Check stored online account credentials
          - add app actions
        """
        super().__init__(application_id=application_id,
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

        self.backend = BankingBackend()
        self.window = None
        self.assistant = None

        # Add assistant action
        action = Gio.SimpleAction.new("assistant", None)
        action.connect("activate", self.on_assistant)
        self.add_action(action)

        # Add refresh action
        action = Gio.SimpleAction.new("refresh", None)
        action.connect("activate", self.on_refresh)
        self.add_action(action)

        # Add lock action
        action = Gio.SimpleAction.new("lock", None)
        action.connect("activate", self.on_lock)
        self.add_action(action)

        # Add about action
        action = Gio.SimpleAction.new("about", None)
        action.connect("activate", self.on_about)
        self.add_action(action)

        self.register()
        self.register_dbus_signal()

    def on_session_lock(self,
                        _connection,
                        _unique_name,
                        _object_path,
                        _interface,
                        _signal,
                        state):
        if state[0]:
            self.window.show_unlock_dialog()

    def register_dbus_signal(self) -> None:
        """
        Register a listener so we get notified about screensaver kicking in
        """
        connection = Gio.Application.get_default().get_dbus_connection()
        self.dbus_subscription_id = connection.signal_subscribe(
            None, "org.gnome.ScreenSaver", "ActiveChanged",
            "/org/gnome/ScreenSaver", None,
            Gio.DBusSignalFlags.NONE, self.on_session_lock,)

    def open_assistant(self):
        """
        Open assistant window and connect to done button.
        """
        self.assistant = BankingAssistant(
            application=self,
            backend=self.backend)
        self.assistant.done_button.connect('clicked', self.on_assistant_done)
        if self.window:
            self.assistant.set_transient_for(self.window)

        self.assistant.show_all()

    def start_assistant(self, user):
        self.open_assistant()

    def open_window(self):
        """
        (Re)open main window based on stored online account information.
        """
        if self.window:
            self.window.destroy()

        self.window = BankingWindow(application=self, backend=self.backend)
        self.window.start_assistant_button.connect('clicked',
                                                   self.start_assistant)
        self.window.present()

    def on_assistant(self, action, value):
        """
        Open assistant
        """
        self.open_assistant()

    def on_refresh(self, action, value):
        """
        Refresh data
        """
        self.backend.refresh_accounts()

    def on_lock(self, action, value):
        """
        Lock window
        """
        self.window.show_unlock_dialog()

    def on_about(self, action, value):
        """
        Open about dialog
        """
        about = AboutDialog()
        about.props.transient_for = self.window
        about.present()

    def on_close(self, action, parameter):
        action.destroy()

    def on_assistant_done(self, button):
        """
        Once assistant done button is clicked, store password in libsecret and
        fire up the main window.
        :param button: done button
        """
        print('Assistant done, store password and refresh accounts')
        self.backend.set_blz(self.assistant.bank_entry.get_text())
        self.backend.set_user(self.assistant.user_entry.get_text())
        self.backend.set_safe_password(
            self.assistant.safe_password_entry.get_text())
        self.backend.set_password(self.assistant.password_entry.get_text(),)
        self.backend.set_server(self.assistant.server_entry.get_text())
        self.backend.refresh_accounts()
        self.window.content_stack.set_visible_child_name('unlock')

    def do_activate(self):
        """
        Activation:
        In case no previous window is open check if stored data is sound. If
        not start assistant otherwise open the main window.
        """
        win = self.props.active_window
        if not win:
            self.open_window()
