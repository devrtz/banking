# Banking

Banking application for small screens

## Introduction
This is a FinTS banking application using python fints as backend. This application
is able to connect to your bank and will show your current balance and transactions.
It has been created for my upcoming Librem 5 smartphone by Purism.

## Screenshots

![screenshot](data/screenshots/banking.png)
